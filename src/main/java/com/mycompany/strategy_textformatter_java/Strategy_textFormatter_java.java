package com.mycompany.strategy_textformatter_java;
// Define the strategy interface
interface TextFormatter {
    String format(String text);
}
// Implement concrete strategy classes
class LowercaseFormatter implements TextFormatter {
    @Override
    public String format(String text) {
        return text.toLowerCase();
    }
}
class UppercaseFormatter implements TextFormatter {
    @Override
    public String format(String text) {
        return text.toUpperCase();
    }
}
class CamelCaseFormatter implements TextFormatter {
    @Override
    public String format(String text) {
        String[] words = text.split(" ");
        StringBuilder camelCaseText = new StringBuilder();

        for (String word : words) {
            camelCaseText.append(word.substring(0, 1).toUpperCase());
            camelCaseText.append(word.substring(1).toLowerCase());
        }

        return camelCaseText.toString();
    }
}
// Create a context class that uses the selected text formatting strategy
class TextProcessor {
    private TextFormatter textFormatter;

    public TextProcessor(TextFormatter textFormatter) {
        this.textFormatter = textFormatter;
    }

    public String processText(String text) {
        return textFormatter.format(text);
    }
}
public class Strategy_textFormatter_java {
    public static void main(String[] args) {
         String text = "Hello, World! This is a Text Formatting Example.";
        // Choose the text formatting strategy at runtime
        TextFormatter chosenTextFormatter = new CamelCaseFormatter();  // You can change this to Lowercase or Uppercase
        TextProcessor textProcessor = new TextProcessor(chosenTextFormatter);
        String formattedText = textProcessor.processText(text);
        System.out.println("Formatted text: " + formattedText);
    }
}
